package az.ingress.ingresssecurity.domain;

public enum  Role {
    MANAGER,
    USER
}
