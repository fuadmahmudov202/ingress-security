package az.ingress.ingresssecurity.aspect;

import az.ingress.ingresssecurity.config.SecurityService;
import az.ingress.ingresssecurity.exception.NotAllowedException;
import az.ingress.ingresssecurity.exception.UsernameNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
@RequiredArgsConstructor
@Order(0)
public class RoleConfigAspect {
    private final SecurityService service;

    @Around("@annotation(az.ingress.ingresssecurity.aspect.HasRole)")
    public Object checkRole(ProceedingJoinPoint joinPoint ) throws Throwable {
        var signuture=(MethodSignature) joinPoint.getSignature();
        var annotation= signuture.getMethod().getAnnotation(HasRole.class);
        if (!service.getCurrentJwtCredentials().getRole().contains(annotation.value())){
            log.info("exception occurs when trying to execute method{}",signuture.getMethod());
            throw new NotAllowedException();
        }
        return joinPoint.proceed();

    }
    @Around("publicMethod() && @within(ann)")
    public Object checkRoleByClass(ProceedingJoinPoint joinPoint ,HasRole ann) throws Throwable {
        if (!service.getCurrentJwtCredentials().getRole().contains(ann.value())){
            log.info("exception occurs when trying to execute method{}",joinPoint.getSignature().getName());
            throw new NotAllowedException();
        }
        return joinPoint.proceed();

    }
    @Pointcut("execution(public * * (..))")
    public void publicMethod(){
        //for Selecting public methods
    }
}
