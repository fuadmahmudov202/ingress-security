package az.ingress.ingresssecurity.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Aspect
@Component
@Slf4j
public class LogAspect {
    HashMap<String,Integer> redis=new HashMap<>();
    @Before("execution(* az.ingress.ingresssecurity.contoller.UserController.*(..))" +
            "&& args(dto,..)")
    public void before(JoinPoint joinPoint, Object dto){
        redis.forEach((s, integer) -> System.out.println("redis response "+s+"-"+integer));
        putToRedis("totalAttempt-"+joinPoint.getSignature().getName());

        log.info("ASPECT: executing method {}, input {}",joinPoint.getSignature().getName(),dto);
    }

    @After("execution(* az.ingress.ingresssecurity.contoller.UserController.*(..))" +
            "&& args(dto,..)")
    public void after(JoinPoint joinPoint, Object dto){
        log.info("ASPECT: success method {}, output {}",joinPoint.getSignature().getName(),dto);
    }

    @AfterReturning(value = "execution(* az.ingress.ingresssecurity.contoller.UserController.*(..))" +
            "&& args(dto,..)",returning = "resp")
    public void afterReturn(JoinPoint joinPoint, Object dto,Object resp){
        putToRedis("success-"+joinPoint.getSignature().getName());

        log.info("ASPECT: success method {}, output {}, response {}",joinPoint.getSignature().getName(),dto,resp);
    }

    @AfterThrowing(value = "execution(* az.ingress.ingresssecurity.contoller.UserController.*(..))" +
            "&& args(dto,..)")
    public void afterThrow(JoinPoint joinPoint, Object dto){
        putToRedis("error-"+joinPoint.getSignature().getName());
    }

    private void putToRedis(String key) {
        if (redis.containsKey(key))
            redis.put(key,redis.get(key)+1);
        else
            redis.put(key,1);
    }

}
