package az.ingress.ingresssecurity.contoller;

import az.ingress.ingresssecurity.dto.UserDto;
import az.ingress.ingresssecurity.service.CourseService;
import az.ingress.ingresssecurity.service.UserDetailService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1")
@Slf4j
public class UserController {
    private final UserDetailService userDetailService;
    private final CourseService courceService;

    @PostMapping("/register")
    public UserDto register(@RequestBody UserDto userDto){
        return userDetailService.register(userDto);
    }
    @PostMapping("/login")
    public String login(@RequestBody UserDto userDto){
        return userDetailService.login(userDto);
    }
    @GetMapping("/test")
    public void test(){
        log.info("geldi");
        courceService.getCourses();
    }

}
