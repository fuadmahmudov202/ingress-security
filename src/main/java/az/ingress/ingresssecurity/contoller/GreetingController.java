package az.ingress.ingresssecurity.contoller;

import az.ingress.ingresssecurity.aspect.HasRole;
import az.ingress.ingresssecurity.domain.Role;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1")
@HasRole(Role.USER)
public class GreetingController {
    @GetMapping("/hello/{id}")
    public ResponseEntity<String> greetings(@PathVariable String id){
        return ResponseEntity.ok("hello");
    }

    @GetMapping("/user")
    public ResponseEntity<String> user(){
        return ResponseEntity.ok("Welcome user");
    }
    @PostMapping("/user")
    public ResponseEntity<String> user2(){
        return ResponseEntity.ok("Welcome user post");
    }
    @GetMapping("/manager")
    public ResponseEntity<String> manager(){
        return ResponseEntity.ok("Welcome manager");
    }
}
