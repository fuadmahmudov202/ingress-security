package az.ingress.ingresssecurity.config.extraConfig;

import az.ingress.ingresssecurity.exception.ErrorResponseDto;
import az.ingress.ingresssecurity.exception.JwtException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.Date;

import static az.ingress.ingresssecurity.exception.UniBootcampErrorCodes.INVALID_CREDENTIALS;

@Slf4j
@Component
public class AuthenticationEntryPointConfigurer  implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        log.info("Respond with unauthorized error {}",authException.getMessage());
//        response.sendError(HttpServletResponse.SC_UNAUTHORIZED,authException.getMessage());
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        ErrorResponseDto errorResponseDto = null;
        errorResponseDto = ErrorResponseDto.builder()
                    .status(400)
                    .code(INVALID_CREDENTIALS.code)
                    .path(request.getRequestURI())
                    .timestamp(OffsetDateTime.now())
                    .message("Bad credentials")
                    .build();
        OutputStream responseStream = response.getOutputStream();
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.writeValue(responseStream,errorResponseDto);
        responseStream.flush();
    }
}
