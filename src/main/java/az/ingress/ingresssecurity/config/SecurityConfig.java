package az.ingress.ingresssecurity.config;

import az.ingress.ingresssecurity.config.extraConfig.AuthenticationEntryPointConfigurer;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;

import java.util.List;

import static org.springframework.http.HttpMethod.*;

@Configuration
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final JwtAuthFilterConfigurerAdapter jwtAuthFilterConfigurerAdapter;
    private final AuthenticationEntryPointConfigurer authenticationEntryPointConfigurer;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().disable();
        http.csrf().disable();
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.authorizeRequests()
                .antMatchers(GET,"v1/user").hasAnyAuthority("USER","MANAGER")
                .antMatchers(POST,"v1/user").hasAuthority("MANAGER")
                .antMatchers("/v1/manager").hasAuthority("MANAGER")
                .antMatchers("/v1/**").permitAll()
                .antMatchers(GET,"v1/hello/**").permitAll()
                .antMatchers("/v1/login/**").permitAll();
        http.logout().disable();
        http.formLogin().disable();
        http.apply(jwtAuthFilterConfigurerAdapter);
        http.exceptionHandling().authenticationEntryPoint(authenticationEntryPointConfigurer);
//        super.configure(http);
    }

}
