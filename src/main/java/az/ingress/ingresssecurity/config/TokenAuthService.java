package az.ingress.ingresssecurity.config;

import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TokenAuthService implements AuthService{
    public static final String AUTHORIZATION="Authorization";
    public static final String BEARER="Bearer";
    private final JWTService jwtService;

    @Override
    public Optional<Authentication> getAuthentication(HttpServletRequest request) {
         var authorization=request.getHeader(AUTHORIZATION);
         String token="";
        if (authorization!=null&&authorization.startsWith(BEARER)) {
            token = authorization.substring(BEARER.length());
            Claims claims= jwtService.parseToken(token);
            Authentication authenticationBearer = jwtService.getAuthenticationBearer(claims);
            return Optional.of(authenticationBearer);
        }else
            return Optional.empty();
    }
}
