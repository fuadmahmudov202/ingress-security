package az.ingress.ingresssecurity.config;

import az.ingress.ingresssecurity.domain.Authority;
import az.ingress.ingresssecurity.domain.User;
import az.ingress.ingresssecurity.exception.UsernameNotFoundException;
import az.ingress.ingresssecurity.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
@Service
@RequiredArgsConstructor
public class BasicAuthService implements AuthService {
    public static final String AUTHORIZATION="Authorization";
    public static final String BASIC="Basic ";
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public Optional<Authentication> getAuthentication(HttpServletRequest request){
        var authorization = request.getHeader(AUTHORIZATION);
        if (authorization!=null&& authorization.startsWith(BASIC)){
            String basic= authorization.substring(BASIC.length());
            System.out.println("authorization"+authorization);
            System.out.println(basic);
            byte[] decode= Base64.getDecoder().decode(basic);
            String[] credentials=new String(decode).split(":");
            if (credentials.length!=2){
                throw new RuntimeException();
            }
            User user= userRepository.findByUsername(credentials[0])
                    .orElseThrow(UsernameNotFoundException::new);
            boolean matches=passwordEncoder.matches(credentials[1], user.getPassword());
            if (matches) {
                Authentication authentication = getAuthenticationBearer(user);
                return Optional.of(authentication);
            }
        }
        return Optional.empty();
    }

    public Authentication getAuthenticationBearer(User user){
        List<?> roles= user.getAuthorities().stream().map(Authority::getRole).collect(Collectors.toList());

        List<GrantedAuthority> authorityList = roles.stream()
                .map(o -> new SimpleGrantedAuthority(o.toString()))
                .collect(Collectors.toList());
        return new UsernamePasswordAuthenticationToken(user,"",authorityList);
    }
}
