package az.ingress.ingresssecurity.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.security.Principal;

@Service
public class SecurityService {

    private final ObjectMapper objectMapper= new ObjectMapper();
    public JwtCredentials getCurrentJwtCredentials(){
        var securityContext= SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();
        return objectMapper.convertValue(authentication.getPrincipal(),JwtCredentials.class);
    }

    public Integer getUserId(){
        return getCurrentJwtCredentials().getUserId();
    }
}
