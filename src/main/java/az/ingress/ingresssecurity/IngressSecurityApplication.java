package az.ingress.ingresssecurity;

import az.ingress.ingresssecurity.config.JWTService;
import az.ingress.ingresssecurity.domain.Authority;
import az.ingress.ingresssecurity.domain.Role;
import az.ingress.ingresssecurity.domain.User;
import az.ingress.ingresssecurity.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Set;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
public class IngressSecurityApplication implements CommandLineRunner {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JWTService jwtService;

    public static void main(String[] args) {
        SpringApplication.run(IngressSecurityApplication.class, args);
    }


    @Override
    public void run(String... args) throws Exception {
        Authority a=new Authority();
        a.setRole(Role.USER);
        User user = User.builder()
                .isAccountNonExpired(true)
                .isAccountNonLocked(true)
                .isCredentialsNonExpired(true)
                .isEnabled(true)
                .username("fuad")
                .password(passwordEncoder.encode("202"))
                .authorities(Set.of(a)).build();
         user = userRepository.save(user);
         log.info("user saved {}",user);
        String s = jwtService.issueToken(user);
        log.info("token generated {}",s);

        Claims claims = jwtService.parseToken(s);
        log.info("token parsed. claims is {}",claims);
    }
}
