package az.ingress.ingresssecurity.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.util.Arrays;

@Getter
public class UniBootcampGenericException extends RuntimeException{
    private final int status;
    private final String code;
    private final String message;
    private final transient Object[] arguments;

    public UniBootcampGenericException(String code,String message,int status,  Object... arguments) {
        super(message);
        this.status = status;
        this.code = code;
        this.message = message;
        this.arguments = arguments==null?new Object[0]:arguments;
    }

    public UniBootcampGenericException(String errorBody, HttpStatus statusCode) {
        super(errorBody);
        this.status = statusCode.value();
        this.code = errorBody;
        this.message = errorBody;
        this.arguments = null;
    }

    @Override
    public String toString() {
        return "UniBootcampGenericException{" +
                "status=" + status +
                ", code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", arguments=" + Arrays.toString(arguments) +
                '}';
    }
}
