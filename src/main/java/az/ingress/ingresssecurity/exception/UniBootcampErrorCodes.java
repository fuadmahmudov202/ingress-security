package az.ingress.ingresssecurity.exception;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum UniBootcampErrorCodes {
    USER_NOT_FOUND("USER-NOT-FOUND"),
    USER_ALREADY_EXIST("USER-ALREADY-EXIST"),
    PASSWORD_DID_NOT_MATCH("PASSWORD-DID-NOT-MATCH"),
    INVALID_CREDENTIALS("INVALID-CREDENTIALS"),
    NOT_ALLOWED("NOT-ALLOWED");
    public final String code;
}
