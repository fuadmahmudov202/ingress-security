package az.ingress.ingresssecurity.exception;

import static az.ingress.ingresssecurity.exception.UniBootcampErrorCodes.NOT_ALLOWED;

public class NotAllowedException extends UniBootcampGenericException {
    public NotAllowedException(Object... arguments) {
        super(NOT_ALLOWED.code, NOT_ALLOWED.code, 400, arguments);
    }
}
