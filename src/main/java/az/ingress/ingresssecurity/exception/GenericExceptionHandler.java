package az.ingress.ingresssecurity.exception;

import az.ingress.ingresssecurity.service.TranslationRepoService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.Date;

import static org.springframework.http.HttpHeaders.ACCEPT_LANGUAGE;

@RestControllerAdvice
@Slf4j
@RequiredArgsConstructor
public class GenericExceptionHandler {
    private final TranslationRepoService translationRepoService;

    @ExceptionHandler(UniBootcampGenericException.class)
    public ResponseEntity<ErrorResponseDto> handleGenericException(UniBootcampGenericException ex, WebRequest webRequest){
        ex.printStackTrace();
        var path= ((ServletWebRequest) webRequest).getRequest().getRequestURL().toString();
        String lang= webRequest.getHeader(ACCEPT_LANGUAGE);
        return createErrorResponse(ex,path,webRequest,lang==null?"en":lang);
    }

    private ResponseEntity<ErrorResponseDto> createErrorResponse(UniBootcampGenericException ex, String path, WebRequest webRequest,String lang) {
        ErrorResponseDto build = ErrorResponseDto.builder()
                .status(ex.getStatus())
                .code(ex.getCode())
                .path(path)
                .timestamp(OffsetDateTime.now())
                .message(translationRepoService.findByKey(ex.getMessage(), lang,ex.getArguments()))
                .detail(translationRepoService.findByKey(ex.getMessage().concat("_DETAIL"),lang,ex.getArguments())).build();
        return ResponseEntity.status(ex.getStatus()).body(build);
    }

}
