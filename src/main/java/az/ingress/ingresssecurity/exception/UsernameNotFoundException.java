package az.ingress.ingresssecurity.exception;

import static az.ingress.ingresssecurity.exception.UniBootcampErrorCodes.USER_NOT_FOUND;

public class UsernameNotFoundException extends UniBootcampGenericException {
    public UsernameNotFoundException( Object... arguments) {
        super(USER_NOT_FOUND.code, USER_NOT_FOUND.code, 404, arguments);
    }

}
