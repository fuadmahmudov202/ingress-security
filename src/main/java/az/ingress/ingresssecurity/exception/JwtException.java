package az.ingress.ingresssecurity.exception;

import org.springframework.http.HttpStatus;

public class JwtException  extends UniBootcampGenericException{
    public JwtException(String code) {
        super(code, HttpStatus.UNAUTHORIZED);
    }
}
