package az.ingress.ingresssecurity.service;

import az.ingress.ingresssecurity.config.SecurityService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class CourseService {
    private final SecurityService securityService;

    public void  getCourses(){
        var jwtCredentials= securityService.getCurrentJwtCredentials();
        log.info("Username is {}",jwtCredentials.getUsername());
        log.info("Username is id {}",securityService.getUserId());
    }
}
