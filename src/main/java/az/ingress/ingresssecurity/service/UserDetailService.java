package az.ingress.ingresssecurity.service;

import az.ingress.ingresssecurity.config.JWTService;
import az.ingress.ingresssecurity.domain.Authority;
import az.ingress.ingresssecurity.domain.Role;
import az.ingress.ingresssecurity.domain.User;
import az.ingress.ingresssecurity.dto.UserDto;
import az.ingress.ingresssecurity.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserDetailService implements UserDetailsService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JWTService jwtService;
    @Autowired
    private KafkaTemplate<String,Object> kafkaTemplate;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("Username "+ username +" Not found"));
    }

    public UserDto register(UserDto userDto) {
        userRepository.findByUsername(userDto.getUsername()).ifPresent(user -> new UsernameNotFoundException("User already exist"));
        Authority a=new Authority();
        a.setRole(Role.USER);
        User user = User.builder()
                .isAccountNonExpired(true)
                .isAccountNonLocked(true)
                .isCredentialsNonExpired(true)
                .isEnabled(true)
                .username(userDto.getUsername())
                .password(passwordEncoder.encode(userDto.getPassword()))
                .authorities(Set.of(a)).build();
        User savedUser = userRepository.save(user);

        kafkaTemplate.send(new ProducerRecord<String,Object>("security-register",0, savedUser.getId().toString(), savedUser));
        return UserDto.builder()
                .id(savedUser.getId())
                .username(savedUser.getUsername()).build();
    }

    public String login(UserDto userDto) {
        User user = userRepository.findByUsername(userDto.getUsername())
                .orElseThrow(az.ingress.ingresssecurity.exception.UsernameNotFoundException::new);
        log.info("dto"+userDto);
        if (passwordEncoder.matches(userDto.getPassword(), user.getPassword()))
            return jwtService.issueToken(user);
        else throw new az.ingress.ingresssecurity.exception.UsernameNotFoundException("User not found");
    }
}
